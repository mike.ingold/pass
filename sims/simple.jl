# Ensure ~/src is in LOAD_PATH so that ModelEngine can be imported
DIR_ROOT = dirname(@__DIR__);
DIR_SRC  = joinpath(DIR_ROOT, "src");
if !( DIR_SRC in LOAD_PATH ) push!(LOAD_PATH, DIR_SRC); end

using Logging
using ModelEngine
using Plots

ENV["JULIA_DEBUG"] = all;

#===============================================================================
                        TIME DOMAIN AND INPUT SIGNALS
===============================================================================#

# Time domain
t_i = 1e-12;
t = ( 0.0 : t_i : 1e-6 );
t_ns = t .* 1e9;

# Input signals
f_clk   = 38.4e6;                       # Clock frequency
T_clk   = 1.0/f_clk;                    # Clock period
T_clk_i = T_clk / t_i;                  # Step index per clock period
ref_clk = sin.((2*pi*f_clk) .* t);      # Generate sinusoidal clock
ref_clk = float.( ref_clk .> 0.0 );     # Convert analog to digital clock

#===============================================================================
                                BLOCKS
===============================================================================#

# Coaxial cable spec for 1 meter RG316 (per L-com datasheet)
coax_rg316_1m = CoaxialCableSpec(1.0, -18.4, 2e8);

# XPCG Phase Shift Table
xpcg_phases_deg = [0.0, 30.0, 60.0, 90.0];
xpcg_phases_i   = map(deg->round(Int, deg*T_clk_i/360.0), xpcg_phases_deg);

# Reference clock source -> coax -> XPCG module -> coax's -> sink's
block_refclk      = SignalSource(ref_clk);
block_refclk_coax = TxMedium(coax_rg316_1m);
block_xpcg        = XPCG(xpcg_phases_i);
block_xpcg_coax   = TxMedium(coax_rg316_1m, 4);
block_xpcg_sinks  = SignalSink(4);

#===============================================================================
                            BLOCK CONNECTIONS
===============================================================================#

# refclk -> coaxial cable -> XPCG
@info "Connecting: refclk -> coaxial cable -> XPCG"
connect(BlockPortAddress(block_refclk), BlockPortAddress(block_refclk_coax));
connect(BlockPortAddress(block_refclk_coax), BlockPortAddress(block_xpcg));

# XPCG outputs -> coaxial cables
@info "Connecting: XPCG -> coaxial cables"
connect(BlockPortAddress(block_xpcg,1), BlockPortAddress(block_xpcg_coax,1));
connect(BlockPortAddress(block_xpcg,2), BlockPortAddress(block_xpcg_coax,2));
connect(BlockPortAddress(block_xpcg,3), BlockPortAddress(block_xpcg_coax,3));
connect(BlockPortAddress(block_xpcg,4), BlockPortAddress(block_xpcg_coax,4));

# coaxial cables -> sinks
@info "Connecting: coaxial cables -> sinks"
connect(BlockPortAddress(block_xpcg_coax,1), BlockPortAddress(block_xpcg_sinks,1));
connect(BlockPortAddress(block_xpcg_coax,2), BlockPortAddress(block_xpcg_sinks,2));
connect(BlockPortAddress(block_xpcg_coax,3), BlockPortAddress(block_xpcg_sinks,3));
connect(BlockPortAddress(block_xpcg_coax,4), BlockPortAddress(block_xpcg_sinks,4));

#===============================================================================
                            DATA VISUALIZATION
===============================================================================#

# Figure 1 - XPCG Output Signals
@info "Plotting results"
plot(title="Simulation XPCG Output Signals", legendtitle="Channel (Phase)", legend=:outertopright)
fig1_labels = map((ch,deg)->"Ch $(ch) ($(deg)°)", 1:4, xpcg_phases_deg);
for i in 1:4
    plot!(t_ns, block_xpcg_sinks.signal_input[i], label=fig1_labels[i])
end
plot!(xlabel="Time (ns)", xlims=(0,150), xticks=0:25:200, xtickfont=font(8,"Arial"))
plot!(ylabel="Voltage (V)", ylims=(-0.1,1.2), yticks=-0.20:0.20:2.0, ytickfont=font(8,"Arial"))
