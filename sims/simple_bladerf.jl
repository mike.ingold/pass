# Free Memory Required: ?

#===============================================================================
                                   PREAMBLE
===============================================================================#

# Ensure ~/src is in LOAD_PATH so that ModelEngine can be imported
DIR_ROOT = dirname(@__DIR__);
DIR_SRC  = joinpath(DIR_ROOT, "src");
if !( DIR_SRC in LOAD_PATH ) push!(LOAD_PATH, DIR_SRC); end

# Data logging
using Logging
using Dates
ENV["JULIA_DEBUG"] = all;

#===============================================================================
                                   START!
===============================================================================#

@info "$(Dates.now())> Building bladeRF test"

using ModelEngine
using Plots

# Time domain
t_i = 1e-12;
t = collect( 0.0 : t_i : 10e-6 );
t_ns = t .* 1e9;

# Generate null signal for unused ports
null_signal = zeros(Float64, length(t));
block_null_source = SignalSource(null_signal);

#===============================================================================
                        PHASED CLOCK GENERATION
===============================================================================#

@info "$(Dates.now())> Building reference clock"

# Reference Clock (REFCLK)
f_clk = 38.4e6;                         # Clock frequency
T_clk = 1.0/f_clk;                      # Clock period
T_clk_i = T_clk / t_i;                  # Step index per clock period
ref_clk = cos.((2*pi*f_clk) .* t);      # Generate sinusoidal clock
ref_clk = float.( ref_clk .> 0.0 );     # Convert analog to digital clock
block_refclk = SignalSource(ref_clk);

#===============================================================================
                                SERVER COMPUTER
===============================================================================#

# Computer-1 Baseband TX Signals
cpu_tx_f   = 8e6;                            # Baseband signal frequency
cpu_tx1_i  = cos.((2*pi*cpu_tx_f) .* t);     # In-phase signal
cpu_tx1_q  = sin.((2*pi*cpu_tx_f) .* t);     # Quadrature signal
cpu_tx2_i  = cos.((2*pi*cpu_tx_f) .* t .+ ((90.0/360.0)*2*pi) );     # In-phase signal
cpu_tx2_q  = sin.((2*pi*cpu_tx_f) .* t .+ ((90.0/360.0)*2*pi) );     # Quadrature signal

# Computer-1 Sources/Sinks
block_cpu1_tx = SignalSource([[cpu_tx1_i]; [cpu_tx1_q]; [cpu_tx2_i]; [cpu_tx2_q]]);
block_cpu1_rx = SignalSink(4);

#===============================================================================
                                   BLADERF-1
===============================================================================#

@info "$(Dates.now())> Building bladeRF"

block_bladeRF1 = BladeRFmicro("Test SDR");

# Connect bladeRF inputs
connect(block_refclk,  1,      block_bladeRF1, portKW[:bladeRF_CLKIN]);
connect(block_cpu1_tx, 1,      block_bladeRF1, portKW[:bladeRF_TX1_I]);
connect(block_cpu1_tx, 2,      block_bladeRF1, portKW[:bladeRF_TX1_Q]);
connect(block_cpu1_tx, 3,      block_bladeRF1, portKW[:bladeRF_TX2_I]);
connect(block_cpu1_tx, 4,      block_bladeRF1, portKW[:bladeRF_TX2_Q]);
connect(block_null_source, 1,  block_bladeRF1, portKW[:bladeRF_RX1]);
connect(block_null_source, 1,  block_bladeRF1, portKW[:bladeRF_RX2]);

# Derive output signals
set_pll(block_bladeRF1, 12);
set_output(block_bladeRF1);

# Connect bladeRF output ports
connect(block_bladeRF1, portKW[:bladeRF_RX1_I],  block_cpu1_rx, 1);
connect(block_bladeRF1, portKW[:bladeRF_RX1_Q],  block_cpu1_rx, 2);
connect(block_bladeRF1, portKW[:bladeRF_RX2_I],  block_cpu1_rx, 3);
connect(block_bladeRF1, portKW[:bladeRF_RX2_Q],  block_cpu1_rx, 4);

#===============================================================================
                                PLOTTING
===============================================================================#

fig_fname = map(i->"simple_bladerf.fig$i.pdf", 1:5);

fig1 = plot(t_ns, block_bladeRF1.signal_input[portKW[:bladeRF_TX1_I]], label="I")
plot!(fig1, t_ns, block_bladeRF1.signal_input[portKW[:bladeRF_TX1_Q]], label="Q",
	title = "TX1 Baseband",
	xlims = (0,500)
     )
savefig(fig1, fig_fname[1])

fig2 = plot(t_ns, block_bladeRF1.signal_input[portKW[:bladeRF_TX2_I]], label="I")
plot!(fig2, t_ns, block_bladeRF1.signal_input[portKW[:bladeRF_TX2_Q]], label="Q",
	title = "TX2 Baseband",
	xlims = (0,500)
     )
savefig(fig2, fig_fname[2])

fig3 = plot(t_ns, real.(block_bladeRF1.rf_carrier), label="I")
plot!(fig3, t_ns, imag.(block_bladeRF1.rf_carrier), label="Q",
        title = "RF Carrier",
	xlims = (0,10),
	xticks = 0:2:10
     )
savefig(fig3, fig_fname[3])

fig4 = plot(t_ns, block_bladeRF1.signal_output[portKW[:bladeRF_TX1]], label="TX1")
plot!(fig4, t_ns, block_bladeRF1.signal_output[portKW[:bladeRF_TX2]], label="TX2",
      title = "TX Output",
      xlims = (0,10),
      xticks = 0:2:10
     )
savefig(fig4, fig_fname[4])

#===============================================================================
                                EPILOGUE
===============================================================================#

@info "$(Dates.now())> Test complete"
