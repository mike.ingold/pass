# Free Memory Required: 6 GB

#===============================================================================
                                   PREAMBLE
===============================================================================#

# Ensure ~/src is in LOAD_PATH so that ModelEngine can be imported
DIR_ROOT = dirname(@__DIR__);
DIR_SRC  = joinpath(DIR_ROOT, "src");
if !( DIR_SRC in LOAD_PATH ) push!(LOAD_PATH, DIR_SRC); end

# Data logging
using Logging
using Dates
log_fn = joinpath(@__DIR__, "pass_builder.log");
log_io = open(log_fn, "a");
logger = SimpleLogger(log_io);
ENV["JULIA_DEBUG"] = all;
global_logger(logger);
println(log_io, "---------------------------------------------"); # visual delimiting

#===============================================================================
                                   START!
===============================================================================#

@info "$(Dates.now())> Building PASS model"

using ModelEngine
using DataFrames
using Feather

# Time domain
t_i = 1e-12;
t = collect( 0.0 : t_i : 10e-6 );

# Transmission media specifications
C = 3e8 * t_i;  # meters/ps
coax_rg316_1m = CoaxialCableSpec(1.0, -18.4, (0.67*C)); # 1 meter RG316 (per L-com datasheet)

# Signal specs
bladeRF_pll_multiplier = 12;
signal_frequency = bladeRF_pll_multiplier * 38.4e6;
λ = 3e8/signal_frequency; # in meters

# Generate null signal for unused ports
null_signal = zeros(Float64, length(t));
block_null_source = SignalSource(null_signal);

flush(log_io);

#===============================================================================
                        PHASED CLOCK GENERATION
===============================================================================#

@info "$(Dates.now())> Building XPCG sub-circuit"

# Reference Clock (REFCLK)
f_clk = 38.4e6;                         # Clock frequency
T_clk = 1.0/f_clk;                      # Clock period
T_clk_i = T_clk / t_i;                  # Step index per clock period
ref_clk = cos.((2*pi*f_clk) .* t);      # Generate sinusoidal clock
ref_clk = float.( ref_clk .> 0.0 );     # Convert analog to digital clock

# XPCG Phase Shift Table
xpcg_phases_deg = [0.0, 30.0, 60.0, 90.0];
xpcg_phases_i   = map(deg->round(Int, deg*T_clk_i/360.0), xpcg_phases_deg);

# Reference clock source -> coax -> XPCG module -> coax's -> sink's
block_refclk      = SignalSource(ref_clk);
block_refclk_coax = TxMedium(coax_rg316_1m);
block_xpcg        = XPCG(xpcg_phases_i);
block_xpcg_coax   = TxMedium(coax_rg316_1m, 4);
block_xpcg_sinks  = SignalSink(4);

# refclk -> coaxial cable -> XPCG
connect(block_refclk, 1,       block_refclk_coax, 1);
connect(block_refclk_coax, 1,  block_xpcg, 1);

# XPCG outputs -> coaxial cables
connect(block_xpcg, 1, block_xpcg_coax, 1);
connect(block_xpcg, 2, block_xpcg_coax, 2);
connect(block_xpcg, 3, block_xpcg_coax, 3);
connect(block_xpcg, 4, block_xpcg_coax, 4);

# coaxial cables -> sinks
connect(block_xpcg_coax, 1, block_xpcg_sinks, 1);
connect(block_xpcg_coax, 2, block_xpcg_sinks, 2);
connect(block_xpcg_coax, 3, block_xpcg_sinks, 3);
connect(block_xpcg_coax, 4, block_xpcg_sinks, 4);

flush(log_io);

#===============================================================================
                                ANTENNA ARRAY
===============================================================================#

# Transmitter array
location_bladeRF_ant1 = (     0.0,  0.0*λ );
location_bladeRF_ant2 = (     0.0,  0.5*λ );
location_bladeRF_ant3 = (     0.0,  1.0*λ );
location_bladeRF_ant4 = (     0.0,  1.5*λ );
location_bladeRF_ant5 = (     0.0,  2.0*λ );

# Client antennas
location_client_ant_1  = (    0.0, 1000.0 );  # endfire
location_client_ant_2  = ( 1000.0,    0.0 );  # broadside

#===============================================================================
                                SERVER COMPUTER
===============================================================================#

# Baseband TX Signals
cpu_tx_f = 8e6;                           # Baseband signal frequency
cpu_txi = cos.((2*pi*cpu_tx_f) .* t);     # In-phase signal
cpu_txq = sin.((2*pi*cpu_tx_f) .* t);     # Quadrature signal

# Computer-1 Sources/Sinks
block_cpu1_tx = SignalSource([[cpu_txi];[cpu_txq]]);
block_cpu1_rx = SignalSink(4);

#===============================================================================
                             SERVER ARRAY
    > Inputs TX1(I,Q) = TX2(I,Q)
    > Inputs RX1 and RX2 unused
    > Output TX2 unused
===============================================================================#

@info "$(Dates.now())> Building bladeRF array"

# Coax and antennas
block_bladeRF_coax = TxMedium(coax_rg316_1m, 5);
block_bladeRF_ant1 = Antenna(location_bladeRF_ant1, 1,1);
block_bladeRF_ant2 = Antenna(location_bladeRF_ant2, 1,1);
block_bladeRF_ant3 = Antenna(location_bladeRF_ant3, 1,1);
block_bladeRF_ant4 = Antenna(location_bladeRF_ant4, 1,1);
block_bladeRF_ant5 = Antenna(location_bladeRF_ant5, 1,1);

# bladeRF-1 -> coax -> antenna
block_bladeRF1 = BladeRFmicro("Array SDR 1");
set_pll(block_bladeRF1, bladeRF_pll_multiplier);
connect(block_xpcg_coax, 1,    block_bladeRF1, portKW[:bladeRF_CLKIN]);
connect(block_cpu1_tx, 1,      block_bladeRF1, portKW[:bladeRF_TX1_I]);
connect(block_cpu1_tx, 2,      block_bladeRF1, portKW[:bladeRF_TX1_Q]);
connect(block_cpu1_tx, 1,      block_bladeRF1, portKW[:bladeRF_TX2_I]);
connect(block_cpu1_tx, 2,      block_bladeRF1, portKW[:bladeRF_TX2_Q]);
connect(block_null_source, 1,  block_bladeRF1, portKW[:bladeRF_RX1]);
connect(block_null_source, 1,  block_bladeRF1, portKW[:bladeRF_RX2]);
connect(block_bladeRF1, portKW[:bladeRF_TX1],  block_bladeRF_coax, 1);
connect(block_bladeRF_coax, 1,  block_bladeRF_ant1, 1);

# bladeRF-2 -> coax -> antenna
block_bladeRF2 = BladeRFmicro("Array SDR 2");
set_pll(block_bladeRF2, bladeRF_pll_multiplier);
connect(block_xpcg_coax, 1,    block_bladeRF2, portKW[:bladeRF_CLKIN]);
connect(block_cpu1_tx, 1,      block_bladeRF2, portKW[:bladeRF_TX1_I]);
connect(block_cpu1_tx, 2,      block_bladeRF2, portKW[:bladeRF_TX1_Q]);
connect(block_cpu1_tx, 1,      block_bladeRF2, portKW[:bladeRF_TX2_I]);
connect(block_cpu1_tx, 2,      block_bladeRF2, portKW[:bladeRF_TX2_Q]);
connect(block_null_source, 1,  block_bladeRF2, portKW[:bladeRF_RX1]);
connect(block_null_source, 1,  block_bladeRF2, portKW[:bladeRF_RX2]);
connect(block_bladeRF2, portKW[:bladeRF_TX1],  block_bladeRF_coax, 2);
connect(block_bladeRF_coax, 2,  block_bladeRF_ant2, 1);

# bladeRF-3 -> coax -> antenna
block_bladeRF3 = BladeRFmicro("Array SDR 3");
set_pll(block_bladeRF3, bladeRF_pll_multiplier);
connect(block_xpcg_coax, 1,    block_bladeRF3, portKW[:bladeRF_CLKIN]);
connect(block_cpu1_tx, 1,      block_bladeRF3, portKW[:bladeRF_TX1_I]);
connect(block_cpu1_tx, 2,      block_bladeRF3, portKW[:bladeRF_TX1_Q]);
connect(block_cpu1_tx, 1,      block_bladeRF3, portKW[:bladeRF_TX2_I]);
connect(block_cpu1_tx, 2,      block_bladeRF3, portKW[:bladeRF_TX2_Q]);
connect(block_null_source, 1,  block_bladeRF3, portKW[:bladeRF_RX1]);
connect(block_null_source, 1,  block_bladeRF3, portKW[:bladeRF_RX2]);
connect(block_bladeRF3, portKW[:bladeRF_TX1],  block_bladeRF_coax, 3);
connect(block_bladeRF_coax, 3,  block_bladeRF_ant3, 1);

# bladeRF-4 -> coax -> antenna
block_bladeRF4 = BladeRFmicro("Array SDR 4");
set_pll(block_bladeRF4, bladeRF_pll_multiplier);
connect(block_xpcg_coax, 1,    block_bladeRF4, portKW[:bladeRF_CLKIN]);
connect(block_cpu1_tx, 1,      block_bladeRF4, portKW[:bladeRF_TX1_I]);
connect(block_cpu1_tx, 2,      block_bladeRF4, portKW[:bladeRF_TX1_Q]);
connect(block_cpu1_tx, 1,      block_bladeRF4, portKW[:bladeRF_TX2_I]);
connect(block_cpu1_tx, 2,      block_bladeRF4, portKW[:bladeRF_TX2_Q]);
connect(block_null_source, 1,  block_bladeRF4, portKW[:bladeRF_RX1]);
connect(block_null_source, 1,  block_bladeRF4, portKW[:bladeRF_RX2]);
connect(block_bladeRF4, portKW[:bladeRF_TX1],  block_bladeRF_coax, 4);
connect(block_bladeRF_coax, 4,  block_bladeRF_ant4, 1);

# bladeRF-5 -> coax -> antenna
block_bladeRF5 = BladeRFmicro("Array SDR 5");
set_pll(block_bladeRF5, bladeRF_pll_multiplier);
connect(block_xpcg_coax, 1,    block_bladeRF5, portKW[:bladeRF_CLKIN]);
connect(block_cpu1_tx, 1,      block_bladeRF5, portKW[:bladeRF_TX1_I]);
connect(block_cpu1_tx, 2,      block_bladeRF5, portKW[:bladeRF_TX1_Q]);
connect(block_cpu1_tx, 1,      block_bladeRF5, portKW[:bladeRF_TX2_I]);
connect(block_cpu1_tx, 2,      block_bladeRF5, portKW[:bladeRF_TX2_Q]);
connect(block_null_source, 1,  block_bladeRF5, portKW[:bladeRF_RX1]);
connect(block_null_source, 1,  block_bladeRF5, portKW[:bladeRF_RX2]);
connect(block_bladeRF5, portKW[:bladeRF_TX1],  block_bladeRF_coax, 5);
connect(block_bladeRF_coax, 5,  block_bladeRF_ant5, 1);

#===============================================================================
                            CLIENT COMPUTER
===============================================================================#

# Client Computer Sources/Sinks
block_cpu2_tx = SignalSource(null_signal, 2);
block_cpu2_rx = SignalSink(4);

#===============================================================================
                                CLIENT SDR
===============================================================================#

@info "$(Dates.now())> Building receiver system"

# Blocks: free space -> antenna -> coax
block_client_ant_1 = Antenna(location_client_ant_1, 5,1);
block_freespace_11 = TxMedium(block_bladeRF_ant1, block_client_ant_1, C, -0.01);
block_freespace_21 = TxMedium(block_bladeRF_ant2, block_client_ant_1, C, -0.01);
block_freespace_31 = TxMedium(block_bladeRF_ant3, block_client_ant_1, C, -0.01);
block_freespace_41 = TxMedium(block_bladeRF_ant4, block_client_ant_1, C, -0.01);
block_freespace_51 = TxMedium(block_bladeRF_ant5, block_client_ant_1, C, -0.01);

# Blocks: free space -> antenna -> coax
block_client_ant_2 = Antenna(location_client_ant_2, 5,1);
block_freespace_12 = TxMedium(block_bladeRF_ant1, block_client_ant_2, C, -0.01);
block_freespace_22 = TxMedium(block_bladeRF_ant2, block_client_ant_2, C, -0.01);
block_freespace_32 = TxMedium(block_bladeRF_ant3, block_client_ant_2, C, -0.01);
block_freespace_42 = TxMedium(block_bladeRF_ant4, block_client_ant_2, C, -0.01);
block_freespace_52 = TxMedium(block_bladeRF_ant5, block_client_ant_2, C, -0.01);

# Connect tx antenna -> free space
connect(block_bladeRF_ant1, 1,  block_freespace_11, 1);
connect(block_bladeRF_ant1, 1,  block_freespace_12, 1);
connect(block_freespace_11, 1,  block_client_ant_1, 1);
connect(block_freespace_12, 1,  block_client_ant_2, 1);
# Connect tx antenna -> free space
connect(block_bladeRF_ant2, 1,  block_freespace_21, 1);
connect(block_bladeRF_ant2, 1,  block_freespace_22, 1);
connect(block_freespace_21, 1,  block_client_ant_1, 2);
connect(block_freespace_22, 1,  block_client_ant_2, 2);
# Connect tx antenna -> free space
connect(block_bladeRF_ant3, 1,  block_freespace_31, 1);
connect(block_bladeRF_ant3, 1,  block_freespace_32, 1);
connect(block_freespace_31, 1,  block_client_ant_1, 3);
connect(block_freespace_32, 1,  block_client_ant_2, 3);
# Connect tx antenna -> free space
connect(block_bladeRF_ant4, 1,  block_freespace_41, 1);
connect(block_bladeRF_ant4, 1,  block_freespace_42, 1);
connect(block_freespace_41, 1,  block_client_ant_1, 4);
connect(block_freespace_42, 1,  block_client_ant_2, 4);
# Connect tx antenna -> free space
connect(block_bladeRF_ant5, 1,  block_freespace_51, 1);
connect(block_bladeRF_ant5, 1,  block_freespace_52, 1);
connect(block_freespace_51, 1,  block_client_ant_1, 5);
connect(block_freespace_52, 1,  block_client_ant_2, 5);

# Connect antenna -> coax
block_client_coax = TxMedium(coax_rg316_1m, 2);
connect(block_client_ant_1, 1,  block_client_coax,  1);
connect(block_client_ant_2, 1,  block_client_coax,  2);

# Client bladeRF
block_client_bladeRF = BladeRFmicro("Client SDR");
set_rx_gain(block_client_bladeRF, 9.0);
set_pll(block_client_bladeRF, bladeRF_pll_multiplier);

# Connect bladeRF inputs
connect(block_xpcg_coax, 1,    block_client_bladeRF, portKW[:bladeRF_CLKIN]);
connect(block_cpu2_tx, 1,      block_client_bladeRF, portKW[:bladeRF_TX1_I]);
connect(block_cpu2_tx, 2,      block_client_bladeRF, portKW[:bladeRF_TX1_Q]);
connect(block_cpu2_tx, 1,      block_client_bladeRF, portKW[:bladeRF_TX2_I]);
connect(block_cpu2_tx, 2,      block_client_bladeRF, portKW[:bladeRF_TX2_Q]);
connect(block_client_coax, 1,  block_client_bladeRF, portKW[:bladeRF_RX1]);
connect(block_client_coax, 2,  block_client_bladeRF, portKW[:bladeRF_RX2]);

# Connect bladeRF output ports
connect(block_client_bladeRF, portKW[:bladeRF_RX1_I],  block_cpu2_rx, 1);
connect(block_client_bladeRF, portKW[:bladeRF_RX1_Q],  block_cpu2_rx, 2);
connect(block_client_bladeRF, portKW[:bladeRF_RX2_I],  block_cpu2_rx, 3);
connect(block_client_bladeRF, portKW[:bladeRF_RX2_Q],  block_cpu2_rx, 4);

set_output(block_client_bladeRF)

#===============================================================================
                                EXPORT DATA
===============================================================================#

# Gather relevant data
data = DataFrame(
                 # Reference signals
                 t = t,
                 refclk = block_refclk.signal_output[1],
                 xpcg1  = block_xpcg.signal_output[1],
                 xpcg2  = block_xpcg.signal_output[2],
                 xpcg3  = block_xpcg.signal_output[3],
                 xpcg4  = block_xpcg.signal_output[4],
                 # Server-side
                 tx_rfc_i = real.(block_bladeRF1.rf_carrier),
                 tx_rfc_q = imag.(block_bladeRF1.rf_carrier),
                 tx_i     = block_cpu1_tx.signal_output[1],
                 tx_q     = block_cpu1_tx.signal_output[2],
                 tx1      = block_bladeRF1.signal_output[portKW[:bladeRF_TX1]],
                 tx2      = block_bladeRF1.signal_output[portKW[:bladeRF_TX2]],
                 # Client-side
                 rx_rfc_i = real.(block_client_bladeRF.rf_carrier),
                 rx_rfc_q = imag.(block_client_bladeRF.rf_carrier),
                 rx1      = block_client_bladeRF.signal_input[portKW[:bladeRF_RX1]],
                 rx1_i    = block_cpu2_rx.signal_input[1],
                 rx1_q    = block_cpu2_rx.signal_input[2],
		 rx2      = block_client_bladeRF.signal_input[portKW[:bladeRF_RX2]],
		 rx2_i    = block_cpu2_rx.signal_input[3],
		 rx2_q    = block_cpu2_rx.signal_input[4]
                )

# Save as a Feather file
Feather.write("pass_model.feather", data);

#===============================================================================
                                EPILOGUE
===============================================================================#

@info "$(Dates.now())> PASS complete"

close(log_io) # Flush writes to log file and close file handler
