#===============================================================================
                                   PREAMBLE
===============================================================================#

# Data logging
using Logging
using Dates
log_fn = joinpath(@__DIR__, "pass_analysis.log");
log_io = open(log_fn, "a");
logger = SimpleLogger(log_io);
ENV["JULIA_DEBUG"] = all;
global_logger(logger);
println(log_io, "---------------------------------------------"); # visual delimiting

#===============================================================================
                                   START!
===============================================================================#

@info "$(Dates.now())> Analyzing PASS data"
flush(log_io)

using DataFrames
import DSP
import Feather
import FFTW
using Plots

# Imported data
data = Feather.read("pass_model.feather");
xpcg_phases_deg = [0.0, 30.0, 60.0, 90.0];

# Plot Fonts
font_title  = Plots.font(family="Arial", pointsize=10.0);
font_small  = Plots.font(family="Arial", pointsize=6.0);

#===============================================================================
                         FIGURE 1 - XPCG OUTPUT SIGNALS
===============================================================================#

@info "$(Dates.now())> Generating Figure 1 (fig1)"

fig1_labels = map((ch,deg)->"Channel $(ch) ($(deg)°)", 1:length(xpcg_phases_deg), xpcg_phases_deg);

fig1 = plot(title="XPCG Output Signals",
	    size = (2000,2000), 
	    xlabel="Time (ns)",
	    ylabel="Voltage (V)"
	   )
plot!(fig1, data.t_ns, data.xpcg1, label=fig1_labels[1])
plot!(fig1, data.t_ns, data.xpcg2, label=fig1_labels[2])
plot!(fig1, data.t_ns, data.xpcg3, label=fig1_labels[3])
plot!(fig1, data.t_ns, data.xpcg4, label=fig1_labels[4])
plot!(fig1, xlims=(0,100), ylims=(-0.05,1.05), legend=:right)
savefig(fig1, "fig1.png")

#===============================================================================
                         FIGURE 2 - BLADERF TRANSMITTER
===============================================================================#

@info "$(Dates.now())> Generating Figure 2 (fig2)"

# Subplot - Baseband
fig2_iq = plot(data.t_ns, data.tx_i, label="I",
               title  = "Baseband",
               xlabel = "Time (ns)",
               xlims  = (0,700),
               xticks = 0:400:4000
              )
plot!(fig2_iq, data.t_ns, data.tx_q, label="Q")

# Subplot - RF Carrier
fig2_rfc = plot(data.t_ns, data.tx_rfc_i, label="I",
                title  = "RF Carrier",
                xlabel = "Time (ns)",
                xlims  = (0,20),
                xticks = 0:2:20
               )
plot!(fig2_rfc, data.t_ns, data.tx_rfc_q, label="Q")

# Subplot - TX Output
fig2_tx1 = plot(data.t_ns, data.tx1, label=false,
                title  = "TX1 Output",
                xlabel = "Time (ns)",
                xlims  = (0,20),
                xticks = 0:2:20,
                ylims  = (-1.05,1.05),
                yticks = -1.0:0.25:1.0
               )

# Composite Plot
fig2 = plot(fig2_iq, fig2_rfc, fig2_tx1,
	    size = (2000,2000), 
	    layout = (3,1),
            titlefont  = font_title,
            legendfont = font_small,
            xtickfont  = font_small,
            ytickfont  = font_small,
            guidefont  = font_small    # Axes labels
           )
savefig(fig2, "fig2.png")

#===============================================================================
                            FIGURE 3 - BLADERF RECEIVER
===============================================================================#

@info "$(Dates.now())> Generating Figure 3 (fig3)"

# Subplot - Received Signal
fig3_rx1 = plot(data.t_ns, data.rx1, label="Inline",
                title="Received Signal",
                xlabel="Time (ns)",
                xlims=(3300,3500)
               )
plot!(fig3_rx1, data.t_ns, data.rx2, label="Perpendicular")

# Subplot - RF Carrier
fig3_rfc = plot(data.t_ns, data.rx_rfc_i, label="I",
                title  = "RF Carrier",
                xlabel = "Time (ns)",
                xlims  = (0,20)
               )
plot!(fig3_rfc, data.t_ns, data.rx_rfc_q, label="Q")

# Subplot - Demodulated Baseband
fig3_iq = plot(data.t_ns, data.rx1_i, label="I",
               title  = "Demodulated Baseband",
               xlabel = "Time (ns)",
               xlims  = (3300,4000)
              )
plot!(fig3_iq, data.t_ns, data.rx1_q, label="Q")

# Composite Plot
fig3 = plot(fig3_rx1, fig3_rfc, fig3_iq, 
	    size = (2000,2000),
	    layout = (3,1),
            titlefont  = font_title,
            legendfont = font_small,
            xtickfont  = font_small,
            ytickfont  = font_small,
            guidefont  = font_small    # Axes labels
           )
savefig(fig3, "fig3.png")

#===============================================================================
                      FIGURE 4 - SERVER SIDE FREQUENCY DOMAIN
===============================================================================#

@info "$(Dates.now())> Generating Figure 4 (fig4)"

# Define frequency domain
t_i = data.t[2] - data.t[1];
F_domain_Hz = FFTW.fftfreq(length(data.t), 1/t_i);
F_domain_MHz = F_domain_Hz .* 1e-6;

# Original IQ signal
txi_F    = FFTW.fft(data.tx_i);
txi_F_dB = DSP.Util.pow2db.(abs.(txi_F));
# Server oscillator
txc_F    = FFTW.fft(data.tx_rfc_i);
txc_F_dB = DSP.Util.pow2db.(abs.(txc_F));
# Transmitted signal
tx1_F    = FFTW.fft(data.tx1);
tx1_F_dB = DSP.Util.pow2db.(abs.(tx1_F));

# Subplots
fig4_txi = plot(F_domain_MHz, txi_F_dB, label=false,
                title  = "Original IQ Signal",
                xlabel = "Frequency (MHz)",
                xlims  = (0,1000),
                ylabel = "Signal Power (dB)",
                ylims  = (0,70)
               )
fig4_txc = plot(F_domain_MHz, txc_F_dB, label=false,
                title  = "TX Oscillator",
                xlabel = "Frequency (MHz)",
                xlims  = (0,1000),
                ylabel = "Signal Power (dB)",
                ylims  = (0,70)
               )
fig4_tx1 = plot(F_domain_MHz, tx1_F_dB, label=false,
                title  = "Transmitted Signal",
                xlabel = "Frequency (MHz)",
                xlims  = (0,1000),
                ylabel = "Signal Power (dB)",
                ylims  = (0,70)
               )

# Composite plot
fig4 = plot(fig4_txi, fig4_txc, fig4_tx1, size=(2000,2000), layout=(3,1))
savefig(fig4, "fig4.png")

#===============================================================================
                      FIGURE 5 - CLIENT SIDE FREQUENCY DOMAIN
===============================================================================#

@info "$(Dates.now())> Generating Figure 5 (fig5)"

# Received signals
rx1_F    = FFTW.fft(data.rx1);
rx1_F_dB = DSP.Util.pow2db.(abs.(rx1_F));
rx2_F    = FFTW.fft(data.rx2);
rx2_F_dB = DSP.Util.pow2db.(abs.(rx2_F));
# Client oscillator
rxc_F    = FFTW.fft(data.rx_rfc_i);
rxc_F_dB = DSP.Util.pow2db.(abs.(rxc_F));
# Recovered IQ signals
rx1_i_F    = FFTW.fft(data.rx1_i);
rx1_i_F_dB = DSP.Util.pow2db.(abs.(rx1_i_F));
rx2_i_F    = FFTW.fft(data.rx2_i);
rx2_i_F_dB = DSP.Util.pow2db.(abs.(rx2_i_F));

# Subplot - Received signals
fig5_rx1 = plot(F_domain_MHz, rx1_F_dB, label="Inline",
                title  = "Received Signal",
                xlabel = "Frequency (MHz)",
                xlims  = (0,1000),
                ylabel = "Signal Power (dB)",
                ylims  = (0,70)
               )
plot!(fig5_rx1, F_domain_MHz, rx2_F_dB, label="Perpendicular")

# Subplot - Client oscillator
fig5_rxc = plot(F_domain_MHz, rxc_F_dB, label=false,
                title  = "RX Oscillator",
                xlabel = "Frequency (MHz)",
                xlims  = (0,1000),
                ylabel = "Signal Power (dB)",
                ylims  = (0,70)
               )

# Subplot - Recovered signals
fig5_rxi = plot(F_domain_MHz, rx1_i_F_dB, label="Inline",
                title  = "Recovered IQ Signal",
                xlabel = "Frequency (MHz)",
                xlims  = (0,1000),
                ylabel = "Signal Power (dB)",
                ylims  = (0,70)
               )
plot!(fig5_rxi, F_domain_MHz, rx2_i_F_dB, label="Perpendicular")

# Composite plot
fig5 = plot(fig5_rx1, fig5_rxc, fig5_rxi, size=(2000,2000), layout=(3,1))
savefig(fig5, "fig5.png")

#===============================================================================
                               EPILOGUE
===============================================================================#

@info "$(Dates.now())> PASS Analysis Complete"

close(log_io) # Flush writes to log file and close file handler
