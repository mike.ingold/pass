/*************************************************************************************************
 *                                 PLOTTING-RELATED FUNCTIONS
 *************************************************************************************************/

// Draw figure
function draw_figure(fig, phases) {
  fig.data_sel.ch1 = fig.data_all[phases[0]/5]; fig.data_sel.ch1.name = "SDR1";
  fig.data_sel.ch2 = fig.data_all[phases[1]/5]; fig.data_sel.ch2.name = "SDR2";
  fig.data_sel.ch3 = fig.data_all[phases[2]/5]; fig.data_sel.ch3.name = "SDR3";
  fig.data_sel.ch4 = fig.data_all[phases[3]/5]; fig.data_sel.ch4.name = "SDR4";
  fig.data_sel.ch5 = fig.data_all[phases[4]/5]; fig.data_sel.ch5.name = "SDR5";

  var data = [fig.data_sel.ch1, fig.data_sel.ch2, fig.data_sel.ch3, fig.data_sel.ch4, fig.data_sel.ch5];
  Plotly.newPlot(fig.id, data, fig.layout, {responsive: true});
  console.log(`Figure ${fig.id} plotted`);
  fig.drawn = true;
}

 // Import data from JSON
 function get_figure_data(fig) {
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {
       fig.data_all = JSON.parse(this.responseText).lines;
       console.log(`Figure ${fig.id} data loaded`);
     }
   };
   xmlhttp.open("GET", fig.data_url, true);
   xmlhttp.send();
 }

 /*************************************************************************************************
  *                                 BUILD FIGURE 1
  *************************************************************************************************/

//var gui_azimuth = document.getElementById("gui_azimuth");

// Define Figure 1 properties
var fig1 = {
  id: 'fig1',
  data_url: "data/clock_all.json",
  data_all: false,
  data_sel: { ch1: false, ch2: false, ch3: false, ch4: false, ch5: false },
  drawn: false,
  layout: {
    title: "XPCG Clock Signal Distribution",
    showlegend: true,
    xaxis: {
      title: 'Time (ns)',
      dtick: 10
    },
    yaxis: {
      title: 'Voltage (V)',
      range: [-0.1, 4.1],
      dtick: 1.0
    }
  }
}

var phase_config =
{
     "0": [  0, 180,   0, 180,   0],
     "5": [345, 160, 340, 160, 340],
    "10": [330, 150, 325, 145, 320],
    "15": [325, 135, 310, 125, 300],
    "20": [320, 125, 295, 105, 275],
    "25": [315, 120, 280,  85, 250],
    "30": [315, 110, 270,  65, 220],
    "35": [320, 105, 255,  40, 190],
    "40": [325, 105, 240,  20, 160],
    "45": [335, 105, 230,   0, 125],
    "50": [350, 105, 220, 335,  90],
    "55": [  5, 105, 210, 315,  55],
    "60": [ 20, 110, 200, 290,  20],
    "65": [ 40, 120, 195, 270, 345],
    "70": [ 65, 125, 190, 250, 310],
    "75": [ 90, 135, 185, 230, 275],
    "80": [120, 150, 180, 210, 245],
    "85": [145, 160, 180, 195, 210],
    "90": [180, 180, 180, 180, 180]
};

// Retrieve data and plot Figure 1
get_figure_data(fig1);
setTimeout(function () { draw_figure(fig1,phase_config[0]) }, 1000);
