/*************************************************************************************************
 *                                 MAPPING-RELATED FUNCTIONS
 *  Reference:
 *    https://leafletjs.com/reference-1.6.0.html#imageoverlay
 *************************************************************************************************/

// Instantiate an imageOverlay object
function get_imageOverlay_handle(struct) {
    if ( struct.handle == false ) {
        struct.handle = L.imageOverlay(struct.url, struct.bounds, struct.options);
        console.log(`Created imageOverlay handler (${struct.url})`)
    }
    else {
        console.log(`imageOverlay (${struct.url}) already has a handle`)
    }
}

// Draw imageOverlay on map
function draw_imageOverlay(struct) {
    if ( struct.handle == false ) {
        throw `Trying to draw imageOverlay (${struct.url}) without existing handler`;
    }
    struct.handle.addTo(mymap);
    struct.visible = true;
    console.log(`imageOverlay (${struct.url}) added to map`)
}

// Change imageOverlay image
function change_imageOverlay(struct, new_url) {
    if ( struct.handle == false ) {
        throw `Trying to change imageOverlay (${struct.url}) without existing handler`;
    }
    struct.url = new_url;
    struct.handle.setUrl(struct.url);
}

/*************************************************************************************************
 *                                BUILD BASE MAP
 *************************************************************************************************/

const map_id = 'mapid';
const map_default_location = L.latLng([33.428243, -111.935804]);

// MapBox Configuration
const mapbox_zoomlvl = 15;
var   mapbox_style = false ? 'mapbox/satellite-v9' : 'mapbox/streets-v11';
const mapbox_api_token = 'pk.eyJ1IjoibWlrZWluZ29sZCIsImEiOiJjazltM3lja3oyY2czM2Vud2NqOTZiam9zIn0.EBd4uKZEwaFp10KAUeLHWQ';
const mapbox_attribution = `Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>
                            contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>,
                            Imagery © <a href="https://www.mapbox.com/">Mapbox</a>`;
const mapbox_url = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}';
var mapbox_settings = {
    attribution: mapbox_attribution,
    maxZoom: 18,
    id: mapbox_style,
    tileSize: 512,
    zoomOffset: -1,
    accessToken: mapbox_api_token
}

// Instantiate the map browser, add a MapBox/OpenStreetMap tile
var mymap = L.map(map_id).setView(map_default_location, mapbox_zoomlvl);
var mymap_basetile = L.tileLayer(mapbox_url, mapbox_settings);
mymap_basetile.addTo(mymap);

/*************************************************************************************************
 *                                    IMAGE OVERLAY
 *************************************************************************************************/

efield_images = {
    "0": "images/efield/efield_0.png",
    "5": "images/efield/efield_5.png",
    "10": "images/efield/efield_10.png",
    "15": "images/efield/efield_15.png",
    "20": "images/efield/efield_20.png",
    "25": "images/efield/efield_25.png",
    "30": "images/efield/efield_30.png",
    "35": "images/efield/efield_35.png",
    "40": "images/efield/efield_40.png",
    "45": "images/efield/efield_45.png",
    "50": "images/efield/efield_50.png",
    "55": "images/efield/efield_55.png",
    "60": "images/efield/efield_60.png",
    "65": "images/efield/efield_65.png",
    "70": "images/efield/efield_70.png",
    "75": "images/efield/efield_75.png",
    "80": "images/efield/efield_80.png",
    "85": "images/efield/efield_85.png",
    "90": "images/efield/efield_90.png"
}

// Overlay - E-field Heatmap
var overlay_efield = {
    handle: false,
    name: "Phased Array E-Field",
    url: 'images/efield/efield_0.png',
    bounds: map_default_location.toBounds(4000),
    options: { opacity: 1 },
    visible: false
}
get_imageOverlay_handle(overlay_efield);
draw_imageOverlay(overlay_efield);

document.addEventListener("DOMContentLoaded", function(event) {
    var slider_azimuth = document.getElementById("gui_azimuth");
    slider_azimuth.oninput = function() {
      change_imageOverlay(overlay_efield, efield_images[this.value]);
      draw_figure(fig1, phase_config[this.value]);
    }
});



/*************************************************************************************************
 *                              ICONS - LOCATIONS OF INTEREST
 *************************************************************************************************/

// Overlay - Sun Devil Stadium
var overlay_stadium = {
    handle: false,
    name: "Sun Devil Stadium",
    url: 'images/sundevil.png',
    bounds: L.latLng([33.426421, -111.932571]).toBounds(200),
    options: { opacity: 1 },
    visible: false
}
get_imageOverlay_handle(overlay_stadium);
draw_imageOverlay(overlay_stadium);

// Overlay - Ira A. Fulton School of Engineering
var overlay_irafulton = {
    handle: false,
    name: "Ira A. Fulton School of Engineering",
    url: 'images/icon_iaf.png',
    bounds: L.latLng([33.423556, -111.939581]).toBounds(125),
    options: { opacity: 1 },
    visible: false
}
get_imageOverlay_handle(overlay_irafulton);
draw_imageOverlay(overlay_irafulton);

// Overlay - Goldwater Center
var overlay_goldwater = {
    handle: false,
    name: "Goldwater Center",
    url: 'images/icon_gwc.png',
    bounds: L.latLng([33.420808, -111.930441]).toBounds(125),
    options: { opacity: 1 },
    visible: false
}
get_imageOverlay_handle(overlay_goldwater);
draw_imageOverlay(overlay_goldwater);
