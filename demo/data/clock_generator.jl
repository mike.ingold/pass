# File system
DIR_DATA = @__DIR__;
DIR_DEMO = dirname(DIR_DATA);
FNAME_JSON = joinpath(DIR_DEMO,"data","clock_all.json");

import JSON

# Generate PlotlyJS JSON object for a line plot
function get_plotlyjs_json_line(x::Vector{T}, y::Vector{T}, name::String)::Dict{String,Any} where {T<:Number}
    return Dict{String,Any}("x"=>x, "y"=>y, "type"=>"scatter", "name"=>name)
end

# Clock characteristics
const f = 38.4e6;
const T = 1.0/f;
const ω = 2*π*f;
const V = 3.3;

# Time domain
t_ns = collect( 0.0 : 0.05 : 100.0 );
t = t_ns .* 1e-9;

# Clock generator
x(A,ω,t,ϕ) = A .* float.(cos.( ω .* t .+ ϕ ) .> 0);

# Background noise
noise = 0.05 .* rand(Float64, length(t));

# Generate signals
xpcg_phases_deg = collect( 0.0 : 5.0 : 355.0 );
xpcg_phases_rad = Base.Math.deg2rad.(xpcg_phases_deg);
clock_signals = map( ϕ->(x(V, ω, t, ϕ) .+ noise), xpcg_phases_rad );

# Convert signals to formatted dict's, then package in a single common dict
json_line_dicts = map( (y,name)->get_plotlyjs_json_line(t_ns, y, name), clock_signals, string.(round.(Int,xpcg_phases_deg)) );
json_lines = Dict{String,Any}("lines"=>json_line_dicts);

# Write outputs to file
open(FNAME_JSON, "w") do io
    JSON.print(io, json_lines, 4);
end;
