# File system
DIR_DATA = @__DIR__;
DIR_DEMO = dirname(DIR_DATA);
DIR_IMGS = joinpath(DIR_DEMO,"images","efield");
FNAME_IMAGE = joinpath(DIR_IMGS,"efield_test.png");

using LinearAlgebra
using Plots

# Signal parameters
const f = 0.25;
const ω = 2*π*f;
const λ = 1/f;
const τ = -1/20.0;

# Spatial parameters
xy_max  = 100.00;
xy_step =   0.05;
x = collect( -xy_max : xy_step : xy_max );
y = collect( -xy_max : xy_step : xy_max );

#===============================================================================
                                  UTILITIES
===============================================================================#

z(A,ω,t,ϕ,τ) = (A .* cos.( ω .* t .+ ϕ )) .* exp.(τ*t);

mutable struct Antenna
	x::Float64
	y::Float64
	ϕ::Float64
end

function get_range_matrix(x::Vector{Float64}, y::Vector{Float64})
	xy_grid = collect( Iterators.product(x,y) );
	return map(ij->hypot(ij...), xy_grid)
end

function get_field_map(antenna::Antenna, ω::Float64)
	global x, y, τ
	map_x  = x .- antenna.x;
	map_y  = y .- antenna.y;
	map_r  = get_range_matrix(map_x, map_y);
	return map( t->z(10.0,ω,t,antenna.ϕ,τ), map_r )
end

function rotate_ant(ant, rads::Float64)
	r  = hypot(ant.x::Float64, ant.y::Float64);
	θ  = atan(ant.y::Float64/ant.x::Float64) + rads::Float64 + ( ant.x::Float64<0.0 ? pi : 0.0 );
	return ( x = r*cos(θ), y = r*sin(θ), ϕ = ant.ϕ )
end

#===============================================================================
                                  BUILD AND ANALYZE
===============================================================================#

# Build antenna array
antennas = Vector{Antenna}( [ 	Antenna( -1.0*λ, -0.5*λ, 0.0 ), Antenna( -1.0*λ,  0.5*λ, 0.0 ),
								Antenna( -0.5*λ, -0.5*λ, 0.0 ), Antenna( -0.5*λ,  0.5*λ, 0.0 ),
								Antenna(    0.0, -0.5*λ, 0.0 ), Antenna(    0.0,  0.5*λ, 0.0 ),
								Antenna(  0.5*λ, -0.5*λ, 0.0 ), Antenna(  0.5*λ,  0.5*λ, 0.0 ),
								Antenna(  1.0*λ, -0.5*λ, 0.0 ), Antenna(  1.0*λ,  0.5*λ, 0.0 )  ] );


θ_range = collect( 0.0 : 5.0 : 90.0 );
fnames = map( θ->"efield_$(round(Int,θ)).png", θ_range );

test_plan_rad = Dict{Float64,Vector{Float64}}();
test_plan_deg = Dict{Float64,Vector{Float64}}();

for θ in θ_range
	# Get new phases for optimal transmission toward azimuth
	new_ϕ = map( ant->dot([cosd(θ),sind(θ)],[ant.x, ant.y])*2*π/λ , antennas );
	new_array = map( (ant,ϕ)->Antenna(ant.x, ant.y, ϕ) , antennas, new_ϕ );

	# Record data used
	test_plan_rad[θ] = new_ϕ;
	test_plan_deg[θ] = map( x->(Base.Math.rad2deg(x)+720)%360, new_ϕ );

	#= Build e-field
	Ez_super = zeros(Float64, length(x), length(y));
	for ant in new_array
		Ez_super .+= get_field_map(ant, ω);
	end

	# Plots
	fig1 = plot(Ez_super,
		    size = (2000,2000),
		    seriestype = :heatmap,
		    xticks = false,
		    yticks = false,
		    color  = cgrad(:bluesreds),
			colorbar = false
		   );

	i = findfirst(==(θ), θ_range);
	savefig(fig1, joinpath(DIR_IMGS, fnames[i]));
	=#
end

println("Complete")

phase_config =
{
     "0": [  0, 180,   0, 180,   0],
     "5": [345, 160, 340, 160, 340],
    "10": [330, 150, 325, 145, 320],
    "15": [325, 135, 310, 125, 300],
    "20": [320, 125, 295, 105, 275],
    "25": [315, 120, 280,  85, 250],
    "30": [315, 110, 270,  65, 220],
    "35": [320, 105, 255,  40, 190],
    "40": [325, 105, 240,  20, 160],
    "45": [335, 105, 230,   0, 125],
    "50": [350, 105, 220, 335,  90],
    "55": [  5, 105, 210, 315,  55],
    "60": [ 20, 110, 200, 290,  20],
    "65": [ 40, 120, 195, 270, 345],
    "70": [ 65, 125, 190, 250, 310],
    "75": [ 90, 135, 185, 230, 275],
    "80": [120, 150, 180, 210, 245],
    "85": [145, 160, 180, 195, 210],
    "90": [180, 180, 180, 180, 180]
};

phase_config_normd = map(x->map(y->round(Int,div(y,5)*5), x), phase_config);
phase_config_dec = map(x->x[1:2:end], phase_config_normd);
