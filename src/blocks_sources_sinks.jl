#===============================================================================
                         BLOCKS - SOURCES & SINKS
===============================================================================#

"""
Block - Signal Sink
"""
mutable struct SignalSink <: AbstractBlockSink
    # I/O Ports (n:0)
    signal_input::Vector{Vector{Float64}}   # Input signal(s)
    # Constructors
    SignalSink(n=1) = new(fill(EMPTY_VECTOR,n));
end

"""
Block - Signal Source
"""
mutable struct SignalSource <: AbstractBlockSource
    # I/O Ports (0:n)
    signal_output::Vector{Vector{Float64}}    # Output signal(s)
    # Constructors
    SignalSource(signal::Vector{Float64}, n::Int=1) = new(fill(signal,n));
    SignalSource(signals::Vector{Vector{Float64}}) = new(signals);
end

#===============================================================================
                              UTILITY FUNCTIONS
===============================================================================#

# Pretty-printing for AbstractBlockSink objects
function Base.show(io::IO, this::SignalSink)
    print(io, "Signal Sink ($(length(this.signal_input)) channel)");
end

# Pretty-printing for AbstractBlockSource objects
function Base.show(io::IO, this::SignalSource)
    print(io, "Signal Source ($(length(this.signal_output)) channel)");
end

#===============================================================================
                              BLOCK FUNCTIONS
===============================================================================#

"""
    set_output(this::SignalSource)
Derive the output signals for a given `Antenna block`.
"""
function set_output(this::SignalSource)
    @debug "$(Dates.now())> Deriving output signals for $(this)"
    return;  # No actions required
end
