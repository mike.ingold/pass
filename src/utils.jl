#===============================================================================
                            UTILITY FUNCTIONS
===============================================================================#

"""
    ac_couple(signal)
Calculate the result of a `signal` with its DC content removed.
"""
function ac_couple(signal::Vector{T})::Vector{T} where {T<:Number}
    return ( signal .- Statistics.mean(signal) )
end

"""
    ac_couple!(signal)
Remove the DC content of a given `signal`.
"""
function ac_couple!(signal::Vector{T}) where {T<:Number}
    signal .-= Statistics.mean(signal);
end

"""
    demodulate(signal, carrier)
Demodulate an `RF signal` with a `quadrature RF carrier wave`.
"""
function demodulate(signal::Vector{Float64}, carrier::Vector{Complex{Float64}})::Vector{Complex{Float64}}
    return complex.((signal .* real.(carrier)), (signal .* imag.(carrier)));
end

"""
    frequency_multiply(signal, ratio)
Transform an `input signal` via frequency multiplication by an `integer multiple ratio`.
"""
function frequency_multiply(signal::Vector{Float64}, ratio::Int)
    # Transform input signal into frequency-domain, split left/right-sides
    clkin_F = FFTW.fft(signal);
    ss_len = round(Int, length(clkin_F)/2, RoundDown);
    clkin_F_ls = clkin_F[1:ss_len];
    clkin_F_rs = clkin_F[end:-1:(end-ss_len+1)];

    # Horizontally dilate the frequency-domain, clipping overflow
    clkrf_F_ls = DSP.resample(clkin_F_ls, ratio)[1:ss_len];
    clkrf_F_rs = DSP.resample(clkin_F_rs, ratio)[1:ss_len];

    # Reconstruct double-sided domain by mirroring
    clkrf_F = zeros(Complex{Float64}, length(clkin_F));
    clkrf_F[1:ss_len] = clkrf_F_ls;
    clkrf_F[end:-1:(end-ss_len+1)] = clkrf_F_rs;

    # Convert back to time-domain
    clkrf_t = FFTW.ifft(clkrf_F);
    return clkrf_t
end

"""
    modulate(baseband, carrier)
Modulate a `baseband IQ signal` with a `quadrature RF carrier wave`.
"""
function modulate(baseband::Vector{Complex{Float64}}, carrier::Vector{Complex{Float64}})::Vector{Float64}
    # Validate that input arguments have identical lengths
    if ( length(baseband) != length(carrier) )
        throw(ArgumentError("Signal length mismatch: baseband ($(length(baseband))) vs carrier ($(length(carrier)))"))
    end

    # Ensure inputs are AC-coupled (copy to avoid modifying source data)
    baseband = ac_couple(baseband);
    carrier  = ac_couple(carrier);

    # Modulate
    return (real.(baseband) .* real.(carrier)) .+ (imag.(baseband) .* imag.(carrier))
end

"""
    phase_shift(signal, phase_angle)
Phase shift a given `complex signal` by a `phase angle in radians`.
"""
function phase_shift(signal::Vector{Complex{Float64}}, phase_angle::Float64)::Vector{Complex{Float64}}
    # Optimization for no-phase-shift condition
    if ( phase_angle == 0.0 ) return signal; end

    phasor = complex(cos(phase_angle), sin(phase_angle));
    return ( signal .* phasor )
end

"""
    phase_shift!(signal, phase_angle)
Phase shift a given `complex signal` by a `phase angle in radians`.
"""
function phase_shift!(signal::Vector{Complex{Float64}}, phase_angle::Float64)
    # Optimization for no-phase-shift condition
    if ( phase_angle == 0.0 ) return; end

    signal .= phase_shift(signal, phase_angle);
end

"""
    phase_shift_deg(signal, phase_angle)
Phase shift a given `complex signal` by a `phase angle in degrees`.
"""
function phase_shift_deg(signal::Vector{Complex{Float64}}, phase_angle::Float64)::Vector{Complex{Float64}}
    return phase_shift(signal, Base.Math.deg2rad(phase_angle))
end

"""
    phase_shift_deg!(signal, phase_angle)
Phase shift a given `complex signal` by a `phase angle in degrees`.
"""
function phase_shift_deg!(signal::Vector{Complex{Float64}}, phase_angle::Float64)
    # Optimization for no-phase-shift condition
    if ( phase_angle == 0.0 ) return; end

    signal .= phase_shift(signal, Base.Math.deg2rad(phase_angle));
end

"""
    phase_shift_clock(signal, phase_angle)
Phase shift an `analog representation of a digital clock signal` by a `phase angle in radians`.
"""
function phase_shift_clock(signal::Vector{Float64}, phase_angle::Float64)::Vector{Float64}
    # Optimization for no-phase-shift condition
    if ( phase_angle == 0.0 ) return signal; end

    # Convert analog signal to digital
    dclock = signal_to_dclock(signal);

    # Build a list of step sizes between `1`s in dclock
    @warn "$(Dates.now())> Danger lies ahead..."
    steps = Vector{Int}();
    i = 1;
    i_last = 0;
    while ( (i = findnext(dclock, i)) != nothing )
        step_size = i - i_last;
        push!(steps, step_size);
        i_last = i;
    end

    # Find the mean step size, trimming small values as switching noise
    filter!(x->(x>10), steps);
    period = Statistics.mean(steps);

    # Estimate discrete-time shift required and return computed signal
    t_shift = round(Int, period * phase_angle / (2*pi));
    return transform(signal, 0, t_shift)
end

"""
    phase_shift_clock_deg(signal, phase_angle)
Phase shift an `analog representation of a digital clock signal` by a `phase angle in degrees`.
"""
function phase_shift_clock_deg(signal::Vector{Float64}, phase_angle::Float64)::Vector{Float64}
    return clock_phase_shift(signal, Base.Math.deg2rad(phase_angle))
end

"""
    signal_to_dclock(signal)
Convert an analog `clock signal` to digital using a 50% threshold
"""
function signal_to_dclock(signal::Vector{Float64})::BitArray{1}
    threshold = 0.5 * ( minimum(signal) + maximum(signal) );
    return ( signal .> threshold )
end

"""
    transform(signal, scale_dB, time_shift)
Transform a `signal` by a `scaling factor` and `discrete-time shift`.
"""
function transform(signal::Vector{Float64}, scale_dB::Float64, time_shift::Int)::Vector{Float64}
    # Validate shift argument
    if ( time_shift < 0 )
        throw(ArgumentError("Shift value ($(time_shift)) must be non-negative/causal"))
    elseif ( time_shift >= length(signal) )
        throw(ArgumentError("Shift value ($(time_shift)) must be within length of the provided signal ($(length(signal)))"))
    end

    # If signal is unchanged, simply pass through original signal without duplication
    if ( ( scale_dB == 0.0 ) && ( time_shift == 0 ) ) return signal; end

    signal_length = length(signal);
    impulse_response = zeros(Float64, signal_length);
    impulse_response[time_shift+1] = DSP.Util.db2pow(scale_dB);
    return DSP.conv(signal, impulse_response)[1:signal_length];     # Trim excess data
end

"""
    transform!(signal, scale_dB, time_shift)
Transform a `signal` by a `scaling factor` and `discrete-time shift`.
"""
function transform!(signal::Vector{Float64}, scale_dB::Float64, time_shift::Int)
    # If signal is unchanged, simply pass through original signal without duplication
    if ( ( scale_dB == 0.0 ) && ( time_shift == 0 ) ) return; end

    signal .= transform(signal, scale_dB, time_shift);
end
