module ModelEngine
    using Dates
    using DSP
    using FFTW
    using Logging
    using Statistics

    import Base: show

    include("blocks.jl")
    export BlockPortAddress, CoaxialCableSpec, portKW
    include("blocks_sources_sinks.jl")
    export SignalSink, SignalSource
    include("blocks_io.jl")
    export Antenna, BladeRFmicro, TxMedium, XPCG

    include("utils.jl")
    export connect, set_output, set_pll, set_rx_gain
end
