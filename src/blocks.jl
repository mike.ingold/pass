#===============================================================================
                   SIGNAL FLOW/PROCESSING BLOCKS
===============================================================================#

# Type Heirarchy
abstract type AbstractBlock end
abstract type AbstractBlockSink   <: AbstractBlock end  # Has: signal_input
abstract type AbstractBlockSource <: AbstractBlock end  # Has: signal_output
abstract type AbstractBlockIO <: AbstractBlock end      # Has: signal_input, signal_output

# Type Unions
AbstractBlockWithInput = Union{AbstractBlockSink,AbstractBlockIO};
AbstractBlockWithOutput = Union{AbstractBlockSource,AbstractBlockIO};

const EMPTY_VECTOR = Vector{Float64}();

# Dictionary for keyword lookup of block ports
portKW = Dict{Symbol,Int}();

"""
Metadata - Address container for a Block:Port combination
"""
mutable struct BlockPortAddress
    block::AbstractBlock
    port::Int
    BlockPortAddress(block, port=1) = new(block, port);
end

"""
Metadata - Specification container for a coaxial cable
"""
struct CoaxialCableSpec
    length::Float64
    loss_per_100m_dB::Float64
    v::Float64
end

"""
Metadata - Connection between two BlockPortAddress's
"""
mutable struct BlockConnection
    from::BlockPortAddress
    to::BlockPortAddress
end

"""
Composite Block Structure
"""
mutable struct CompositeBlock
    blocks::Vector{AbstractBlock}
    connections::Vector{BlockConnection}
    signal_input::Vector{Vector{Float64}}
    signal_output::Vector{Vector{Float64}}
end

#===============================================================================
                              BLOCK FUNCTIONS
===============================================================================#

"""
    connect(from::BlockPortAddress, to::BlockPortAddress)
Connect an output port `at one address` to an input port `at another address`.
"""
function connect(from::BlockPortAddress, to::BlockPortAddress)
    set_output(from.block)

    if !( typeof(from.block) <: AbstractBlockWithOutput)
        @error "$(Dates.now())> From block is a $(typeof(from.block))"
        throw(ArgumentError("From-block must have output ports"))
    elseif !( typeof(to.block) <: AbstractBlockWithInput)
        @error "$(Dates.now())> To block is a $(typeof(to.block))"
        throw(ArgumentError("To-block must have input ports"))
    end

    to.block.signal_input[to.port] = from.block.signal_output[from.port];
end

"""
    connect(from_block, from_port, to_block, to_port)
Connect a `output port` from `a block` to an `input port` on `another block`.
"""
function connect(from_block::AbstractBlockWithOutput, from_port::Int,
                 to_block::AbstractBlockWithInput,    to_port::Int    )
    connect(BlockPortAddress(from_block, from_port), BlockPortAddress(to_block, to_port));
end
