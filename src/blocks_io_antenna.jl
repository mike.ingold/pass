"""
Block - Antenna
"""
mutable struct Antenna{T} <: AbstractBlockIO where {T<:Number}
    # Properties
    location_x::T                           # Location (x)
    location_y::T                           # Location (y)
    # I/O Ports (n:n)
    signal_input::Vector{Vector{Float64}}   # Input signals
    signal_output::Vector{Vector{Float64}}  # Output signals
end

"""
    Antenna(location, in, out)
Construct an Antenna at a `physcial location` with a given number of `input`,`output` ports.
"""
function Antenna(location::Tuple{T,T}, n_in::Int, n_out::Int)::Antenna{T} where {T<:Number}
    # Validate number of input:output ports
    if !in(1, [n_in,n_out])
        throw(ArgumentError("Antenna ports must be 1:n or n:1"))
    end
    return Antenna(location[1], location[2], fill(EMPTY_VECTOR,n_in), fill(EMPTY_VECTOR,n_out));
end

#===============================================================================
                              UTILITY FUNCTIONS
===============================================================================#

# Pretty-printing for Antenna objects
function Base.show(io::IO, this::Antenna)
    print(io, "Antenna at location ($(this.location_x),$(this.location_y))");
end

#===============================================================================
                              BLOCK FUNCTIONS
===============================================================================#

"""
    set_output(this::Antenna)
Derive the output signals for a given `Antenna block`.
"""
function set_output(this::Antenna)
    # Only proceed if all input ports are connected to something
    if any( this.signal_input .== fill(EMPTY_VECTOR, length(this.signal_input)) )
        @warn "$(Dates.now())> Derivation of output signals for $(this) abstained"
        return
    else
        @debug "$(Dates.now())> Deriving output signals for $(this)"
    end

    n_in = length(this.signal_input);
    n_out = length(this.signal_output);

    if ( n_in == 1 )
        for i in 1:n_out
            this.signal_output[i] = this.signal_input[1];
        end
    elseif ( n_out == 1 )
        this.signal_output[1] = sum(this.signal_input);
    else
        throw(ArgumentError("Antenna ports must be configured as n:1 or 1:n"))
    end
end
