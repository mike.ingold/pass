"""
Block - Experimental Phased Clock Generator
"""
mutable struct XPCG <: AbstractBlockIO
    # Properties
    time_shifts::Vector{Int}                # Discrete-time shift table
    # I/O Ports (1:n)
    signal_input::Vector{Vector{Float64}}   # Input signal [CLK]
    signal_output::Vector{Vector{Float64}}  # Output signal(s) [CLK_OUT_1, ..., CLK_OUT_n]
    # Constructors
    XPCG(time_shifts::Vector{Int}) = new(time_shifts, [EMPTY_VECTOR], fill(EMPTY_VECTOR,length(time_shifts)));
    XPCG(n::Int) = new(zeros(Int,n), [EMPTY_VECTOR], fill(EMPTY_VECTOR,n));
end

#===============================================================================
                                  PORT MAP
===============================================================================#

portKW[:XPCG_CLKIN] = 1;

#===============================================================================
                              UTILITY FUNCTIONS
===============================================================================#

# Pretty-printing for XPCG objects
function Base.show(io::IO, this::XPCG)
    n_channels = length(this.signal_output);
    print(io, "XPCG ($(n_channels) channel, $(this.time_shifts))");
end

#===============================================================================
                              BLOCK FUNCTIONS
===============================================================================#

"""
    set_output(this::XPCG)
Derive the output signals for a given `XPCG block`.
"""
function set_output(this::XPCG)
    @debug "$(Dates.now())> Deriving output signals for $(this)"

    # Generate output signals from reference clock with time delays per shift table
    this.signal_output = transform.([this.signal_input[portKW[:XPCG_CLKIN]]], 0.0, this.time_shifts)
end

"""
    set_phases(this::XPCG, time_shifts)
Set the `phase shift table` in an XPCG block.
"""
function set_phases(this::XPCG, time_shifts::Vector{Int})
    @debug "$(Dates.now())> Setting XPCG phase table for $(this)"

    # Validate that shift values are non-negative/causal and within array index of input clock
    if any( time_shifts .< 0 )
        throw(ArgumentError("Shift values must be non-negative/causal"))
    elseif any( time_shifts .>= length(this.signal_input[portKW[:XPCG_CLKIN]]) )
        throw(ArgumentError("Shift values must be within length of the reference clock signal"))
    end

    # Set phase shifts with validated input, recalculate the block's output
    this.time_shifts = phases;
    set_output(this);
end
