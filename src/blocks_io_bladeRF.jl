"""
Block - Nuand bladeRF micro xA4/xA9
"""
mutable struct BladeRFmicro <: AbstractBlockIO
    dev_id::String                          # Identifying name/UUID
    pll_freq_ratio::Int					    # Frequency multiplier ratio (clk->RF)
    rf_carrier::Vector{Complex{Float64}}    # RF carrier wave produced by transceiver
    rx_gain::Float64
    phases::Any                             # TODO specify NamedTuple type
    # I/O Ports (7:6)
    signal_input::Vector{Vector{Float64}}   # Input signals
    signal_output::Vector{Vector{Float64}}  # Output signals
end

"""
    BladeRFmicro(dev_id)
Construct a BladeRFmicro with descriptive `device ID`.
"""
function BladeRFmicro(dev_id::String)::BladeRFmicro
    pll_freq_ratio = 1;
    rf_carrier = EMPTY_VECTOR;
    rx_gain = 0.0;
    phases = (rx1=0.0, rx2=0.0, tx1=0.0, tx2=0.0);
    signal_input = fill(EMPTY_VECTOR, 7);
    signal_output = fill(EMPTY_VECTOR, 6);

    return BladeRFmicro(dev_id, pll_freq_ratio, rf_carrier, rx_gain, phases, signal_input, signal_output);
end

#===============================================================================
                                  PORT MAP
===============================================================================#

# BladeRFmicro input port map
portKW[:bladeRF_TX1_I] = 1;
portKW[:bladeRF_TX1_Q] = 2;
portKW[:bladeRF_TX2_I] = 3;
portKW[:bladeRF_TX2_Q] = 4;
portKW[:bladeRF_RX1]   = 5;
portKW[:bladeRF_RX2]   = 6;
portKW[:bladeRF_CLKIN] = 7;

# BladeRFmicro output port map
portKW[:bladeRF_RX1_I] = 1;
portKW[:bladeRF_RX1_Q] = 2;
portKW[:bladeRF_RX2_I] = 3;
portKW[:bladeRF_RX2_Q] = 4;
portKW[:bladeRF_TX1]   = 5;
portKW[:bladeRF_TX2]   = 6;

#===============================================================================
                            UTILITY FUNCTIONS
===============================================================================#

# Pretty-printing for BladeRFmicro objects
function Base.show(io::IO, this::BladeRFmicro)
    print(io, "bladeRF-micro ($(this.dev_id))");
end

"""
    bladeRF_iq_to_real(signal_i, signal_q, phase, carrier)
Take an `IQ signal pair`, apply a `phase shift`, modulate (IQ->real) with a `carrier wave`.
"""
function bladeRF_iq_to_real(signal_i::Vector{T}, signal_q::Vector{T}, phase::T, carrier::Vector{Complex{T}}) where {T<:Float64}
    signal_iq::Vector{Complex{Float64}} = phase_shift(complex.(signal_i, signal_q), phase);
    signal_real::Vector{Float64} = modulate(signal_iq, carrier);
    return signal_real
end

"""
    bladeRF_real_to_iq(signal_iq, phase, carrier)
Take a `real signal`, demodulate (real->IQ) with a `carrier wave`, and apply a `phase shift`.
"""
function bladeRF_real_to_iq(signal::Vector{T}, carrier::Vector{Complex{T}}, gain::T, phase::T) where {T<:Float64}
    # input -> amplifier -> ADC [-1V,1V] -> IQ demodulator -> IQ phase shifter
    signal = transform(signal, gain, 0);
    clamp!(signal, -1.0, 1.0);
    signal_iq::Vector{Complex{Float64}} = demodulate(signal, carrier);
    phase_shift!(signal_iq, phase);
    return signal_iq
end

#===============================================================================
                            BLOCK FUNCTIONS
===============================================================================#

"""
    set_output(this::BladeRFmicro)
Derive the output signals for a given `BladeRFmicro block`.
"""
function set_output(this::BladeRFmicro)
    # Only proceed if all input ports are connected to something
    if any( this.signal_input .== fill(EMPTY_VECTOR, length(this.signal_input)) )
        @warn "$(Dates.now())> Derivation of output signals for $(this) abstained"
        return
    else
        @debug "$(Dates.now())> Deriving output signals for $(this)"
    end

    # Ensure RF carrier signal is derived properly
    set_rf_carrier(this);

    # Input ports TX1_I/TX1_Q --> output port TX1
    this.signal_output[portKW[:bladeRF_TX1]] =
        bladeRF_iq_to_real(this.signal_input[portKW[:bladeRF_TX1_I]],
                           this.signal_input[portKW[:bladeRF_TX1_Q]],
                           this.phases.tx1, this.rf_carrier);

    # Input ports TX2_I/TX2_Q --> output port TX2
    this.signal_output[portKW[:bladeRF_TX2]] =
        bladeRF_iq_to_real(this.signal_input[portKW[:bladeRF_TX2_I]],
                           this.signal_input[portKW[:bladeRF_TX2_Q]],
                           this.phases.tx1, this.rf_carrier);

    # RX Lowpass Filter (Alias Removal)
    freq_cutoff = 38.4e6 * this.pll_freq_ratio;
    t_i = 1e-12;
    filter_lowpass_spec = DSP.Filters.Lowpass(freq_cutoff, fs=1.0/t_i);    # DSP.Filters.Lowpass(0.0008);
    filter_lowpass_method = DSP.Filters.Butterworth(4);
    filter_lowpass = DSP.Filters.digitalfilter(filter_lowpass_spec, filter_lowpass_method);

    # Input port RX1 --> output ports RX1_I/RX1_Q
    signal_rx1_iq = bladeRF_real_to_iq(this.signal_input[portKW[:bladeRF_RX1]],
                                       this.rf_carrier, this.rx_gain, this.phases.rx1);
    signal_rx1_iq = DSP.Filters.filtfilt(filter_lowpass, signal_rx1_iq);
    this.signal_output[portKW[:bladeRF_RX1_I]] = real.(signal_rx1_iq);
    this.signal_output[portKW[:bladeRF_RX1_Q]] = imag.(signal_rx1_iq);

    # Input port RX2 --> output ports RX2_I/RX2_Q
    signal_rx2_iq = bladeRF_real_to_iq(this.signal_input[portKW[:bladeRF_RX1]],
                                       this.rf_carrier, this.rx_gain, this.phases.rx1);
    signal_rx2_iq = DSP.Filters.filtfilt(filter_lowpass, signal_rx2_iq);
    this.signal_output[portKW[:bladeRF_RX2_I]] = real.(signal_rx2_iq);
    this.signal_output[portKW[:bladeRF_RX2_Q]] = imag.(signal_rx2_iq);
end

"""
    set_phases(this::BladeRFmicro, rx1, rx2, tx1, tx2)
Set the phase shift table in a `BladeRFmicro block` to `provided values in radians`.
"""
function set_phases(this::BladeRFmicro, rx1::T, rx2::T, tx1::T, tx2::T) where {T<:Float64}
    @debug "$(Dates.now())> Configuring RX/TX phase table for $(this)"

    # Validate phase arguments
    if any( [rx1,rx2,tx1,tx2] .< 0 ) throw(ArgumentError("Phases must be non-negative")); end

    this.phases.rx1 = rx1;
    this.phases.rx2 = rx2;
    this.phases.tx1 = tx1;
    this.phases.tx2 = tx2;
end

"""
    set_phases(this::BladeRFmicro)
Set the phase shift table in a `BladeRFmicro block` to default zeros.
"""
function set_phases(this::BladeRFmicro)
    @debug "$(Dates.now())> Configuring RX/TX phase table for $(this)"

    this.phases.rx1 = 0.0;
    this.phases.rx2 = 0.0;
    this.phases.tx1 = 0.0;
    this.phases.tx2 = 0.0;
end

"""
    set_bladeRF_pll(this::BladeRFmicro, multiplier)
Set the internal PLL RF:clock `frequency multiplier ratio` in a `BladeRFmicro block`.
"""
function set_pll(this::BladeRFmicro, multiplier::Int)
    # Validate that multiplier is positive
    if ( multiplier <= 0 ) throw(ArgumentError("Invalid PLL multiplier ($(multiplier))")); end

    @debug "$(Dates.now())> Setting PLL multiplier for $(this)"
    this.pll_freq_ratio = multiplier;
end

"""
    set_bladeRF_rfcarrier(this::BladeRFmicro, multiplier)
Derive the RF carrier wave produced by the transceiver in a `BladeRFmicro block`.
"""
function set_rf_carrier(this::BladeRFmicro)
    @debug "$(Dates.now())> Deriving RF carrier wave for $(this)"
    @warn  "$(Dates.now())> set_rfcarrier currently hardcoded for phase alignment to refclk"

    # TODO
    # My attempt at frequency multiplication of the reference clock signal are in a "pre-success" state.
    # A fragment of the last attempt is maintained below for reference.
    # The following code is a hack to derive a new clock signal given knowledge of the PLL multiplier.
    # CAUTION: this won't provide phase alignment to any bladeRF not operating at the reference clock phase.

    # TODO
    # Empirical measurements revealed that bladeRF-micro generate their RF carrier wave by frequency
    # multiplication in a transceiver PLL based on the selected reference clock. At low multiples the
    # carrier wave retains a fairly square-wave characteristic. As the PLL ratio increases, the generated
    # carrier wave becomes sinusoidal and attenuates increasingly. This behavior can likely be modeled
    # by applying a low-order lowpass filter to the generated carrier.

    # Time domain
    t_i = 1e-12;
    t = ( 0.0 : t_i : 10e-6 );

    # Frequency and period of desired RF carrier
    f = 38.4e6 * this.pll_freq_ratio;
    T = 1.0/f;

    # Generate quadrature pair
    carrier_i = cos.((2*pi*f) .* t);
    carrier_q = sin.((2*pi*f) .* t);
    this.rf_carrier = complex.(carrier_i, carrier_q);

    #=
    clkrf = frequency_multiply(this.signal_input[portKW[:bladeRF_CLKIN]], this.pll_freq_ratio);
    # Rescale output signal to proper range, create IQ signal pair
    #     Note: -( extrema()... ) -> -(min,max) -> min-max
    range_clkin = - -( extrema(this.signal_input[portKW[:bladeRF_CLKIN]])... );
    range_clkrf = - -( extrema(clkrf)... );
    rf_carrier_i  = ( range_clkin / range_clkrf ) .* clkrf;
    rf_carrier_q  = phase_shift_clock(rf_carrier_i, (pi/2.0));
    # Store RF carrier wave IQ signals
    this.rf_carrier = complex.(rf_carrier_i, rf_carrier_q);
    =#
end

function set_rx_gain(this::BladeRFmicro, gain::Float64)
    if ( ( gain < 0.0 ) || ( gain > 50.0 ) )
        @warn "$(Dates.now())> Gain setting ($(gain)) being clamped to hardware rails [0.0, 50.0]"
        gain = clamp(gain, 0.0, 50.0)
    end

    @debug "$(Dates.now())> Setting $(this) RX gain to $(gain) dB"
    this.rx_gain = gain;
end
