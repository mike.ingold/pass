"""
Block - Homogeneous signal transmission medium (e.g. coaxial cable or free space)
"""
mutable struct TxMedium <: AbstractBlockIO
    # Properties
    length::Float64                         # Length of medium
    v::Float64                              # Speed of light
    insertion_loss_dB::Float64              # Insertion loss in dB (absolute)
    # I/O Ports (n:n)(parallel bus)
    signal_input::Vector{Vector{Float64}}   # Input signals
    signal_output::Vector{Vector{Float64}}  # Output signals
    # Constructors
    TxMedium(length::Float64, v::Float64, insertion_loss_dB::Float64, bus_width::Int) =
        new(length, v, insertion_loss_dB, fill(EMPTY_VECTOR, bus_width), fill(EMPTY_VECTOR, bus_width));
end

"""
    TxMedium(cable::CoaxialCableSpec)
Construct a transmission medium based on a provided `coaxial cable specification`.
"""
function TxMedium(cable::CoaxialCableSpec, bus_width::Int=1)
    insertion_loss = cable.loss_per_100m_dB * cable.length / 100.0;
    return TxMedium(cable.length, cable.v, insertion_loss, bus_width)
end

"""
    TxMedium(from::Antenna, to::Antenna, v, loss_dB_per_meter)
Construct a transmission medium with length derived from two `Antennas`, provided `speed of light`
and `insertion loss (dB) per meter`.
"""
function TxMedium(from::Antenna{T}, to::Antenna{T}, v::Float64, loss_dB_per_meter::Float64) where {T<:Number}
    loc_from = float.((from.location_x, from.location_y));
    loc_to   = float.((to.location_x,   to.location_y  ));
    distance = hypot( ( loc_from .- loc_to )... );
    total_loss = distance * loss_dB_per_meter;
    return TxMedium(distance, v, total_loss, 1)
end

#===============================================================================
                              UTILITY FUNCTIONS
===============================================================================#

# Pretty-printing for TxMedium objects
function Base.show(io::IO, this::TxMedium)
    width_in = length(this.signal_input);
    width_out = length(this.signal_output);
    print(io, "Transmission Medium ($(width_in):$(width_out), length $(this.length))");
end

#===============================================================================
                              BLOCK FUNCTIONS
===============================================================================#

"""
    set_output(this::TxMedium)
Derive the output signals for a given `TxMedium block`.
"""
function set_output(this::TxMedium)
    # Only proceed if all input ports are connected to something
    if any( this.signal_input .== fill(EMPTY_VECTOR, length(this.signal_input)) )
        @warn "$(Dates.now())> Derivation of output signals for $(this) abstained"
        return
    else
        @debug "$(Dates.now())> Deriving output signals for $(this)"
    end

    # Transform input signals to output signals in parallel
    Δt = round(Int, (this.length / this.v));
    scale = -abs(this.insertion_loss_dB);
    this.signal_output = transform.(this.signal_input, scale, Δt);
end
