# Phased Array SDR Simulator (PASS)

PASS is a system, written primarily in [Julia](https://julialang.org/), for modeling & simulation of SDR-based phased array radio systems. The ModelEngine can be used to arrange signal pipelines similar to GNU Radio or Simulink.

## Model Engine

The core of PASS is a module called ModelEngine. This engine can be called in Julia with simply:

```julia
using ModelEngine
```

## Simuation

Existing simulations are stored in the `sims` directory.

The BladeRFmicro block models uses an internal DSP pipeline tuned to emulate non-ideal operational characteristics exhibited by the actual hardware in empirical measurements.

Simulation of over-the-air radio signal propagation is currently extremely simplistic, characterized only by a user-provided estimate of an equivalent transmission line with insertion loss in dB.

## License

[MIT](https://choosealicense.com/licenses/mit/)
